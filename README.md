# AlamofireMVC

This is the sample project to  communicate with the server with 3 Layer

## Built using
 - Xcode 9.2
 - Swift 4.0
 
 ## Description
 - This project uses three layers to communicate with the server (Model View Controller) 
 Your code will be arranged using these three layers
 
## Screen Shots
![marty-mcfly](https://gitlab.com/farshid7720/alamofiremvc/-/blob/master/Screen%20Shot%202019-03-27%20at%2014.07.53.png)

![marty-mcfly](https://gitlab.com/farshid7720/alamofiremvc/blob/master/Screen%20Shot%202019-03-27%20at%2014.07.57.png)
